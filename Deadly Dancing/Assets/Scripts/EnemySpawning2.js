﻿ var enemy:Transform;
 private var Timer:float;
 
 function Awake() {
     Timer = Time.time + 1;
 }
 
 function Update() {
     if (Timer < Time.time) { //This checks wether real time has caught up to the timer
         Instantiate(enemy); //This spawns the emeny
         Timer = Time.time + 3; //This sets the timer 3 seconds into the future
     }
 }
