﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	private Animator m_Anim; 
	public AudioClip hurtsound;
	AudioSource audio;
	// Use this for initialization
	void Start () {
		m_Anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ("right")) {
			m_Anim.SetTrigger("right"); 
		}
		if (Input.GetKeyDown ("left")) {
			m_Anim.SetTrigger ("left");
		}
		if (Input.GetKeyDown ("up")) {
			m_Anim.SetTrigger ("up");
		}
	}
}