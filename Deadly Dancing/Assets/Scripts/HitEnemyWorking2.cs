﻿using UnityEngine;
using System.Collections;

public class HitEnemyWorking2 : MonoBehaviour {
	public bool  isActive = false;
	public AudioClip Punch;
	AudioSource audio;
	
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {	
		
		if (Input.GetKeyDown("left"))
		{
			if (isActive == false)
				isActive = true;
		}
		if (Input.GetKeyUp ("left"))
		{
			isActive = false;
		}
		if (Input.GetKeyDown ("up")) {
			if (isActive == false)
				isActive = true;
		}
		if (Input.GetKeyUp ("up")) {
			isActive = false;
		}
	}
	
	void OnTriggerStay2D(Collider2D other) 
	{
		if (isActive == true)
		if (other.gameObject.tag == "enemy") {
			DestroyObject (other.gameObject);
			AudioSource audio = GetComponent<AudioSource>();
			audio.PlayDelayed(0.1f);// Do your active trigger business here.
		}
		if (isActive == true)
		if (other.gameObject.tag == "pressed") {
			DestroyObject (other.gameObject, 0.1f);
		}
		if (isActive == true)
		if (other.gameObject.tag == "Cop") {
			DestroyObject (other.gameObject, 0.5f);
		}
	}
}
