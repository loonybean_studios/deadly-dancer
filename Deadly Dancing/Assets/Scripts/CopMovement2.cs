﻿using UnityEngine;
using System.Collections;

public class CopMovement2 : MonoBehaviour {
	
	private Vector2 startPoint ;
	
	
	public float speed ;
	public float counter;
	private float counter2;
	
	void Start () 
	{
		startPoint = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		counter2 -= Time.deltaTime;
		if ( counter2 < 0)
		{
			RandomPosition ();
			counter2 = counter;
			
		}
		
	}
	
	void RandomPosition ()
	{
		float counter = speed * Time.deltaTime;
		float x = Random.Range(0f, -100f);
		float y = Random.Range(0f, 0f);
		Vector2 endPoint = new Vector2(x, y);
		transform.position = Vector2.Lerp(transform.position, endPoint, Time.deltaTime * speed);  
	}
}