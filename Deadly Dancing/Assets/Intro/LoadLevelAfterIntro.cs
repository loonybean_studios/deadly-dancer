﻿using UnityEngine;
using System.Collections;

public class LoadLevelAfterIntro : MonoBehaviour {

	public MovieTexture DeadlyDancingStoryboard_1;
	
	void Start()
	{
		DeadlyDancingStoryboard_1.Play();
		StartCoroutine("waitForMovieEnd");
	}
	
	IEnumerator waitForMovieEnd()
	{
		
		while(DeadlyDancingStoryboard_1.isPlaying) // while the movie is playing
		{
			yield return new WaitForEndOfFrame();
		}
		// after movie is not playing / has stopped.
		onMovieEnded();
	}
	
	void onMovieEnded()
	{
		Debug.Log("Movie Ended!");
		Application.LoadLevel(1);
	}
}